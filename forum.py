import os

from werkzeug.contrib.fixers import ProxyFix

from api import create_app

from api.common.database import db_session, drop_db
from api.common.models import User, Post

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {
        'db_session': db_session,
        'drop_db': drop_db,
        'User': User,
        'Post': Post
    }


def run():
    host = os.environ.get('APP_HOST', '0.0.0.0')
    port = int(os.environ.get('APP_PORT', 8000))

    app.run(host=host, port=port)


wsgi = ProxyFix(app.wsgi_app)

if __name__ == '__main__':
    run()
