from . import bp
from flask import request, jsonify
from . import domain


@bp.route('', methods=['POST'])
def create_user():
    user_data = request.json
    return domain.create_user(user_data)


@bp.route('/<user_id>', methods=['GET'])
def get_user(user_id):
    return domain.get_user_by_id(user_id)


@bp.route('/all', methods=['GET'])
def get_users():
    return domain.get_all_users()


@bp.route('/<user_id>', methods=['PUT'])
def update_user(user_id):
    user_data = request.json
    return domain.update_user(user_data, user_id)


@bp.route('/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    domain.delete_user(user_id)
    return jsonify({
        'message': f'User with `id: {user_id}` has been deleted.'
    })


@bp.route('/<user_id>/group', methods=['POST'])
def add_group_to_user(user_id):
    return domain.add_group_to_user(request.json, user_id)
