from ..common.models import User, Group
from sqlalchemy.orm.exc import NoResultFound
from ..common.exceptions import RecordNotFound, UserIsAlreadyPartOfGroup


def create_user(user_data):
    print(f"api.bp_user.backend.create_user user_data is: {user_data}")
    user = User.new_from_dict(user_data)
    user.save()
    return user


def get_user_by_id(user_id):
    try:
        result = User.query.filter(User.id == user_id).one()
    except NoResultFound:
        msg = f"There is no user with id {user_id}"
        raise RecordNotFound(message=msg)
    return result


def get_all_users():
    users = User.query.all()
    return users


def update_user(user_data, user_id):
    user = get_user_by_id(user_id)
    user.update_from_dict(user_data)
    user.save()
    return user


def delete_user(user_id):
    user = get_user_by_id(user_id)
    user.delete()


def add_group_to_user(user_data, user_id):
    user = get_user_by_id(user_id)
    group = Group.query.filter(Group.id == user_data["group_id"]).one()
    if group != user.groups.filter(Group.id == user_data["group_id"]).one_or_none():
        user.groups.append(group)
        user.save()
    else:
        msg = "User % s is already part of the " "group % s." % (
            user_id,
            user_data["group_id"],
        )
        raise UserIsAlreadyPartOfGroup(message=msg)
    return user
