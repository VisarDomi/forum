from . import backend
import json


def create_user(user_data):
    user = backend.create_user(user_data)
    return user.to_json(max_nesting=2)


def get_user_by_id(user_id):
    user = backend.get_user_by_id(user_id)
    user_json = user.to_json(max_nesting=2)
    print('api.bp_user.domain.get_user_by_id '
          'type of user_json is %s' % type(user_json))  # <class 'str'>
    return user_json


def get_all_users():
    users = backend.get_all_users()
    list_of_users = [
        user.to_dict(max_nesting=2) for user in users
    ]
    json_dump_of_list_of_users = json.dumps(list_of_users, default=str)
    print('api.bp_user.domain.get_all_users type of json_dump_of_list_of_users'
          ' is %s' % type(json_dump_of_list_of_users))  # <class 'str'>
    return json_dump_of_list_of_users


def update_user(user_data, user_id):
    user = backend.update_user(user_data, user_id)
    return user.to_json(max_nesting=2)


def delete_user(user_id):
    backend.delete_user(user_id)


def add_group_to_user(user_data, user_id):
    user = backend.add_group_to_user(user_data, user_id)

    return user.to_json(max_nesting=2)
