from sqlathanor import AttributeConfiguration, relationship, Column, Table

from sqlalchemy import Integer, String, ForeignKey, DateTime
# from sqlalchemy.orm import backref

from ..common.database import BaseModel, CustomBase
from datetime import datetime


user_group = Table(
    'user_group', BaseModel.metadata,
    Column(
        'user_id',
        Integer,
        ForeignKey('users.id'),
        supports_dict=(True, True),
        supports_json=(True, True)
    ),
    Column(
        'group_id',
        Integer,
        ForeignKey('groups.id'),
        supports_dict=(True, True),
        supports_json=(True, True)
    )
)


class User(BaseModel, CustomBase):
    __tablename__ = 'users'
    __serialization__ = [
        AttributeConfiguration(name='id',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        #                                     put=false, get=true
        AttributeConfiguration(name='email',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='posts',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='groups',
                               supports_dict=(True, True),
                               supports_json=(True, True))
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String, unique=True)

    posts = relationship('Post', back_populates='user', lazy='dynamic')
    groups = relationship(
        'Group',
        secondary='user_group',
        back_populates='users',
        lazy='dynamic'
    )

    def __repr__(self):
        return f"User(id='{self.id}', email='{self.email}')"


class Post(BaseModel, CustomBase):
    __tablename__ = 'posts'
    __serialization__ = [
        AttributeConfiguration(name='id',
                               supports_dict=(False, True),
                               supports_json=(False, True)),
        AttributeConfiguration(name='timestamp',
                               supports_dict=(False, True),
                               supports_json=(False, True)),
        AttributeConfiguration(name='body',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='user',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='user_id',
                               supports_dict=(True, False),
                               supports_json=(True, False))
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    timestamp = Column(DateTime, default=datetime.utcnow)
    body = Column(String)

    user = relationship('User', back_populates='posts')
    user_id = Column(Integer, ForeignKey('users.id'))

    def __repr__(self):
        return f"Post(id='{self.id}', body='{self.body}')"


class Group(BaseModel, CustomBase):
    __tablename__ = 'groups'
    __serialization__ = [
        AttributeConfiguration(name='id',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='name',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
        AttributeConfiguration(name='users',
                               supports_dict=(True, True),
                               supports_json=(True, True)),
    ]

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)

    users = relationship(
        'User',
        secondary='user_group',
        back_populates='groups',
        lazy='dynamic'
    )

    def __repr__(self):
        return f"Group(id='{self.id}', name='{self.name}')"
