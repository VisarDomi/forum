# import json
# from functools import singledispatch
from flask import Response
from flask import jsonify, request
from ..exceptions import JSONException, InvalidAPIRequest
from werkzeug.exceptions import NotFound


# @singledispatch
# def to_serializable(rv):
#     pass


# @to_serializable.register(dict)
# def ts_dict(rv):  # to_dict?
#     return jsonify(rv)


# @to_serializable.register(list)
# def ts_list(rv):  # to_list?
#     return Response(json.dumps(rv))


class JSONResponse(Response):
    charset = 'utf-8'
    default_status = 200
    default_mimetype = 'application/json'
#     @classmethod
#     def force_type(cls, rv, environ=None):
#         rv = to_serializable(rv)
#         return super(JSONResponse, cls).force_type(rv, environ)


def json_error_handler(app):
    @app.errorhandler(JSONException)
    def handle_invalid_usage(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    @app.errorhandler(NotFound.code)
    def resource_not_found(error):
        msg = f'The requested URL `{request.path}` was '\
            'not found on the server.'
        response = jsonify(InvalidAPIRequest(message=msg).to_dict())
        response.status_code = NotFound.code
        return response
