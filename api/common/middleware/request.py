from ..database import db_session
from sqlalchemy.exc import DatabaseError


def commit_session(response):
    if response.status_code >= 400:
        return response
    try:
        db_session.commit()
    except DatabaseError:
        db_session.rollback()
    return response


def shutdown_session(exception=None):
    db_session.remove()
