from sqlathanor import declarative_base

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy_utils import database_exists, create_database, drop_database
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.exc import DatabaseError
from config import Config

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)

db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine)
)


class CustomBase(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def save(self):
        db_session.add(self)
        self._flush()
        return self

    def delete(self):
        db_session.delete(self)
        self._flush()

    def _flush(self):
        try:
            db_session.flush()
        except DatabaseError:
            db_session.rollback()


BaseModel = declarative_base()
BaseModel.query = db_session.query_property()


def init_db():
    if not database_exists(engine.url):
        create_database(engine.url)
    BaseModel.metadata.create_all(bind=engine)


def drop_db():
    BaseModel.metadata.drop_all(bind=engine)
    drop_database(engine.url)
