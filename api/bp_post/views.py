from . import bp
from flask import request, jsonify
from . import domain


@bp.route("", methods=["POST"])
def create_post():
    return domain.create_post(request.json)


@bp.route("/<post_id>", methods=["GET"])
def get_post(post_id):
    return domain.get_post_by_id(post_id)


@bp.route("/all", methods=["GET"])
def get_posts():
    return domain.get_all_posts()


@bp.route("/<post_id>", methods=["PUT"])
def update_post(post_id):
    return domain.update_post(request.json, post_id)


@bp.route("/<post_id>", methods=["DELETE"])
def delete_post(post_id):
    domain.delete_post(post_id)
    return jsonify({"message": f"Post with `id: {post_id}` has been deleted."})
