from . import backend
import json


def create_post(post_data):
    post = backend.create_post(post_data)
    return post.to_json(max_nesting=2)


def get_post_by_id(post_id):
    post = backend.get_post_by_id(post_id)
    post_json = post.to_json(max_nesting=2)
    print('api.bp_post.domain.get_post_by_id '
          'type of post_json is %s' % type(post_json))  # <class 'str'>
    return post_json


def get_all_posts():
    posts = backend.get_all_posts()
    list_of_posts = [
        post.to_dict(max_nesting=2) for post in posts
    ]
    json_dump_of_list_of_posts = json.dumps(list_of_posts, default=str)
    print('api.bp_post.domain.get_all_posts type of json_dump_of_list_of_posts'
          ' is %s' % type(json_dump_of_list_of_posts))  # <class 'str'>
    return json_dump_of_list_of_posts


def update_post(post_data, post_id):
    post = backend.update_post(post_data, post_id)
    return post.to_json(max_nesting=2)


def delete_post(post_id):
    backend.delete_post(post_id)
