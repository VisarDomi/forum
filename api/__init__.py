from config import Config
from flask import Flask
from .bp_user import bp as user_bp
from .bp_post import bp as post_bp
from .bp_group import bp as group_bp
from .common.middleware import response
from .common.middleware import before_request_middleware
from .common.middleware import after_request_middleware
from .common.middleware import teardown_appcontext_middleware
from .common.database import init_db


def create_app(config_class=Config):

    #
    app = Flask(__name__)

    #
    app.config.from_object(config_class)

    #
    app.register_blueprint(user_bp, url_prefix='/api/user')
    app.register_blueprint(post_bp, url_prefix='/api/post')
    app.register_blueprint(group_bp, url_prefix='/api/group')

    app.response_class = response.JSONResponse

    before_request_middleware(app=app)

    after_request_middleware(app=app)

    teardown_appcontext_middleware(app=app)

    response.json_error_handler(app=app)

    init_db()

    return app
