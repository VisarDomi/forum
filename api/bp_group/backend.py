from ..common.models import Group, User
from sqlalchemy.orm.exc import NoResultFound
from ..common.exceptions import RecordNotFound, UserIsAlreadyPartOfGroup


def create_group(group_data):
    print(f'api.bp_group.backend.create_group group_data is: {group_data}')
    group = Group.new_from_dict(group_data)
    group.save()

    return group


def get_group_by_id(group_id):
    try:
        result = Group.query.filter(Group.id == group_id).one()
    except NoResultFound:
        msg = f'There is no group with id {group_id}'
        raise RecordNotFound(message=msg)

    return result


def get_all_groups():
    groups = Group.query.all()

    return groups


def update_group(group_data, group_id):
    print(f'api.bp_group.backend.update_group group_data is: {group_data}')
    group = get_group_by_id(group_id)
    group.update_from_dict(group_data)
    group.save()

    return group


def delete_group(group_id):
    group = get_group_by_id(group_id)
    group.delete()


def add_user_to_group(group_data, group_id):
    print(f'api.bp_group.backend.add_user_to_group group_data is: {group_data}')
    print(f'api.bp_group.backend.add_user_to_group group_id is: {group_id}')
    group = get_group_by_id(group_id)
    user = User.query.filter(User.id == group_data['user_id']).one()
    print(f'api.bp_group.backend.add_user_to_group user is: {user}')
    if user != group.users.filter(User.id == group_data['user_id']).one_or_none():
        # pass
        group.users.append(user)
        group.save()
    else:
        msg = "User % s is already part of the "\
              "group % s." % (group_data['user_id'], group_id)
        raise UserIsAlreadyPartOfGroup(message=msg)
    return group
