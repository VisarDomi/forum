from . import bp
from flask import request, jsonify
from . import domain


@bp.route('', methods=['POST'])
def create_group():
    group_data = request.json

    return domain.create_group(group_data)


@bp.route('/<group_id>', methods=['GET'])
def get_group(group_id):

    return domain.get_group_by_id(group_id)


@bp.route('/all', methods=['GET'])
def get_groups():

    return domain.get_all_groups()


@bp.route('/<group_id>', methods=['PUT'])
def update_group(group_id):
    group_data = request.json
 
    return domain.update_group(group_data, group_id)


@bp.route('/<group_id>', methods=['DELETE'])
def delete_group(group_id):
    domain.delete_group(group_id)

    return jsonify({
        'message': f'Group with `id: {group_id}` has been deleted.'
    })


@bp.route('/<group_id>/user', methods=['POST'])
def add_user_to_group(group_id):
    group_data = request.json
    return domain.add_user_to_group(group_data, group_id)
